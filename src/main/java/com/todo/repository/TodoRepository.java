package com.todo.repository;

import com.todo.model.ToDoItem;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TodoRepository extends JpaRepository<ToDoItem, Long> {
    List<ToDoItem> findByComplete(boolean complete);
}