package com.todo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.SpringBootConfiguration;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@ComponentScan
@EnableJpaRepositories("com.todo.repository")
@EntityScan("com.todo.model")
@SpringBootConfiguration
@EnableAutoConfiguration
public class TodoBackendAppApplication {

	public static void main(String[] args) {
		SpringApplication.run(TodoBackendAppApplication.class, args);
	}

}
