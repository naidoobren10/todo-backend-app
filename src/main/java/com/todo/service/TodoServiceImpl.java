package com.todo.service;

import com.todo.model.ToDoItem;
import com.todo.repository.TodoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class TodoServiceImpl implements TodoService {

    @Autowired
    private TodoRepository todoRepository;

    @Override
    public List<ToDoItem> getAllTodoItems() {
        return todoRepository.findAll();
    }

    @Override
    public List<ToDoItem> getTodoItemsByStatus(boolean status) {
        return todoRepository.findByComplete(status);
    }

    @Override
    public ToDoItem addToDoItem(ToDoItem toDoItem) {
        return todoRepository.save(toDoItem);
    }

    @Override
    public boolean deleteTodoItem(long id) {
        if(todoRepository.existsById(id)){
          todoRepository.deleteById(id);
          return true;
        }
        return false;
    }

    @Override
    public Optional<ToDoItem>  updateTodoItem(ToDoItem toDoItem) {
        Optional<ToDoItem> optional = Optional.ofNullable((ToDoItem)todoRepository.getOne(toDoItem.getId()));
        if(optional.isPresent()){
            if(!optional.get().getDescription().equals("")){
                optional.get().setDescription(toDoItem.getDescription());
                optional.get().setComplete(toDoItem.isComplete());
                todoRepository.save(optional.get());
            }
            else{
                todoRepository.deleteById(optional.get().getId());
            }

        }
        return optional;
    }

    @Override
    public List<ToDoItem> deleteCompletedItems() {
        List<ToDoItem> completedToDoItems = todoRepository.findByComplete(true);
        for(ToDoItem completedItem : completedToDoItems){
            deleteTodoItem(completedItem.getId());
        }
        return getAllTodoItems();
    }


}
