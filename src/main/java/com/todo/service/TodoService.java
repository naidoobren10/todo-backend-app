package com.todo.service;

import com.todo.model.ToDoItem;

import javax.swing.text.html.Option;
import java.util.List;
import java.util.Optional;

public interface TodoService {

     List<ToDoItem> getAllTodoItems();

     List<ToDoItem> getTodoItemsByStatus(boolean status);

     ToDoItem addToDoItem(ToDoItem toDoItem);

     boolean deleteTodoItem(long id);

     Optional<ToDoItem> updateTodoItem(ToDoItem todoitem);

     List<ToDoItem> deleteCompletedItems();
}
