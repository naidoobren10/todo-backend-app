package com.todo.controller;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.todo.model.ToDoItem;
import com.todo.service.TodoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;


@RestController @CrossOrigin(origins = "https://todo-frontend-app-27.herokuapp.com")
@RequestMapping("/todo")
@Component
public class ToDoListController {
    @Autowired
    private TodoService service;

    @GetMapping("/getAllTodoItems")
    public List<ToDoItem> getActiveItems(){
        return service.getAllTodoItems();

    }

    @GetMapping("/getTodoItemsByStatus/{status}")
    public List<ToDoItem> getTodoItemsByStatus(@PathVariable(value = "status") Boolean status){
        return service.getTodoItemsByStatus(status);
    }

    @PostMapping("/addItem")
    public ResponseEntity<?> addItem(@RequestBody ToDoItem toDoItem ){
        try{
            return new ResponseEntity<>(service.addToDoItem(toDoItem), HttpStatus.CREATED);
        } catch(Exception e){
            return errorResponse();
        }
    }

    @DeleteMapping("/deleteTodoItem/{id}")
    public ResponseEntity<?> deleteTodoItem(@PathVariable(value = "id") Long id)  {
         try{
             if(!service.deleteTodoItem(id)){
                 return new ResponseEntity<>("Item Not Found",HttpStatus.NOT_FOUND);
             }
             return new ResponseEntity<>(id, HttpStatus.OK);
          }catch(Exception e){
             return errorResponse();
         }
    }

    @JsonIgnoreProperties(value={"hibernateLazyInitializer", "handler", "fieldHandler"})
    @PostMapping("/updateTodoItem")
    public ResponseEntity<?> updateTodoItem(@RequestBody ToDoItem toDoItem ) {
        try{
            Optional<ToDoItem> optionalToDoItem = service.updateTodoItem(toDoItem);
            if(optionalToDoItem.isPresent()){
                ToDoItem updatedItem = optionalToDoItem.get();
                return new ResponseEntity<>(updatedItem,HttpStatus.OK);
            }else{
                return new ResponseEntity<>("Todo item cannot be found", HttpStatus.NOT_FOUND);

            }

        }catch(Exception e){
            return errorResponse();
        }
    }

    @DeleteMapping("/deleteCompletedTodoItems")
    public List<ToDoItem> deleteCompletedTodoItems() {
        return service.deleteCompletedItems();
    }

    private ResponseEntity<String> errorResponse(){
        return new ResponseEntity<>("Something went wrong :(", HttpStatus.INTERNAL_SERVER_ERROR);
    }
}
