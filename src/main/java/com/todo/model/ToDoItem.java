package com.todo.model;

import javax.persistence.*;

@Entity
@Table(name = "todo_list_items")
public class ToDoItem {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private long id;

    @Column(name = "item_description")
    private String description;

    @Column(name = "item_complete")
    private boolean complete;

    public ToDoItem(){

    }

    public ToDoItem(String description, boolean complete){
        this.description = description;
        this.complete = complete;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getId() {
        return id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean isComplete() {
        return complete;
    }

    public void setComplete(boolean complete) {
        this.complete = complete;
    }

}
