package com.todo;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.todo.controller.ToDoListController;
import com.todo.model.ToDoItem;
import com.todo.repository.TodoRepository;
import com.todo.service.TodoService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.skyscreamer.jsonassert.JSONAssert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import static org.junit.Assert.assertEquals;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.when;


@RunWith(SpringRunner.class)
@WebMvcTest(ToDoListController.class)
public class TodoControllerTests {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private TodoService todoService;

    @MockBean
    private TodoRepository todoRepository;

    @Test
    public void getAllItemsTest() throws Exception {
        ToDoItem toDoItem = new ToDoItem();
        toDoItem.setId(1l);
        toDoItem.setDescription("Todo Item 1");
        toDoItem.setComplete(false);

        List<ToDoItem> toDoItems = new ArrayList<>();
        toDoItems.add(toDoItem);
        when(todoService.getAllTodoItems()).thenReturn(toDoItems);

        MvcResult result = mockMvc.perform(MockMvcRequestBuilders.get("/todo/getAllTodoItems")
                .accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();
        String expected = "[{\"id\":1,\"description\":\"Todo Item 1\",\"complete\":false}]";
        JSONAssert.assertEquals(expected, result.getResponse().getContentAsString(), false);

    }

    @Test
    public void addItemTest() throws Exception {
        String uri = "/todo/addItem";
        ToDoItem toDoItem = new ToDoItem();
        toDoItem.setId(1l);
        toDoItem.setDescription("Todo Item 1");
        toDoItem.setComplete(false);

        String inputJson = mapToJson(toDoItem);
        MvcResult result = mockMvc.perform(MockMvcRequestBuilders.post(uri)
                .contentType(MediaType.APPLICATION_JSON_VALUE).content(inputJson)).andReturn();
        assertEquals(201, result.getResponse().getStatus());

    }

  /*  @Test
    public void updateTodoItem() throws Exception{

        String uri = "/todo/updateTodoItem";

        ToDoItem toDoItem = constructTodoItem(1l, "test update item",false);

        ToDoItem updatedTodoItem = constructTodoItem(1l, "updatedItem",false);
        given(todoService.updateTodoItem(toDoItem)).willReturn(Optional.of(updatedTodoItem));

        String inputJson = mapToJson(toDoItem);

        MvcResult result = mockMvc.perform(MockMvcRequestBuilders.post(uri)
                .contentType(MediaType.APPLICATION_JSON).content(inputJson)).andReturn();
        assertEquals(200, result.getResponse().getStatus());
    }*/

    @Test
    public void deleteItemTest() throws Exception {
        String uri = "/todo/deleteTodoItem/1";
        when(todoService.deleteTodoItem(1l)).thenReturn(true);

        MvcResult result = mockMvc.perform(MockMvcRequestBuilders.delete(uri)
                .contentType(MediaType.APPLICATION_JSON)).andReturn();
        assertEquals("1", result.getResponse().getContentAsString());
        assertEquals(200, result.getResponse().getStatus());

    }

    @Test
    public void deleteCompletedItemsTest() throws Exception {
        String uri = "/todo/deleteCompletedTodoItems";
        ToDoItem toDoItem = new ToDoItem();
        toDoItem.setId(1l);
        toDoItem.setDescription("Incomplete Items 1");
        toDoItem.setComplete(false);

        List<ToDoItem> toDoItems = new ArrayList<>();
        toDoItems.add(toDoItem);
        when(todoService.deleteCompletedItems()).thenReturn(toDoItems);

        MvcResult result = mockMvc.perform(MockMvcRequestBuilders.delete(uri)
                .contentType(MediaType.APPLICATION_JSON_VALUE)).andReturn();
        String expected = "[{\"id\":1,\"description\":\"Incomplete Items 1\",\"complete\":false}]";
        JSONAssert.assertEquals(expected, result.getResponse().getContentAsString(), false);

    }


    protected String mapToJson(Object obj) throws JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.writeValueAsString(obj);
    }

    public ToDoItem constructTodoItem(Long id, String description, boolean complete){
        ToDoItem item = new ToDoItem();
        item.setId(id);
        item.setDescription(description);
        item.setComplete(complete);
        return item;
    }

}
