package com.todo;

import com.todo.model.ToDoItem;
import com.todo.repository.TodoRepository;
import com.todo.service.TodoService;
import com.todo.service.TodoServiceImpl;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.times;
import static org.junit.Assert.assertEquals;
import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.when;

public class TodoServiceTests {

    @InjectMocks
    TodoService service = new TodoServiceImpl();

    @Mock
    TodoRepository todoRepository;

    @Before
    public void init(){
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void retrieveAllTodoItemsTest(){
        List<ToDoItem> toDoItems = new ArrayList<ToDoItem>();

        ToDoItem itemOne = new ToDoItem();
        itemOne.setId(1l);
        itemOne.setDescription("Todo Item 1");
        itemOne.setComplete(false);

        ToDoItem itemTwo = new ToDoItem();
        itemTwo.setId(2l);
        itemTwo.setDescription("Todo Item 2");
        itemTwo.setComplete(false);

        toDoItems.add(itemOne);
        toDoItems.add(itemTwo);

        when(todoRepository.findAll()).thenReturn(toDoItems);

        List<ToDoItem> expectedList = service.getAllTodoItems();

        assertEquals(2, expectedList.size());
        verify(todoRepository,times(1)).findAll();
    }

    @Test
    public void getTodoItemsByStatus(){
        List<ToDoItem> toDoItems = new ArrayList<ToDoItem>();
        ToDoItem itemOne = constructTodoItem(1l,"todoItem 1",true);
        toDoItems.add(itemOne);

        when(todoRepository.findByComplete(true)).thenReturn(toDoItems);

        List<ToDoItem> expectedTodoItems = service.getTodoItemsByStatus(true);

        assertEquals(1l, expectedTodoItems.get(0).getId());
        assertEquals("todoItem 1", expectedTodoItems.get(0).getDescription());
        assertEquals(true, expectedTodoItems.get(0).isComplete());
    }

    @Test
    public void addTodoItem(){
        ToDoItem newTodoItem = constructTodoItem(1l,"new todo",true);

        service.addToDoItem(newTodoItem);
        verify(todoRepository,times(1)).save(newTodoItem);
    }

    @Test
    public void deleteItem(){
        ToDoItem deleteTodoItem = constructTodoItem(1l,"updated todo",true);

        when(todoRepository.existsById(deleteTodoItem.getId())).thenReturn(true);
        service.deleteTodoItem(deleteTodoItem.getId());
        verify(todoRepository,times(1)).deleteById(deleteTodoItem.getId());
    }

    @Test
    public void updateItem(){
        ToDoItem updateTodoItem = constructTodoItem(1l,"updated todo",true);

        when(todoRepository.getOne(1l)).thenReturn(updateTodoItem);
        service.updateTodoItem(updateTodoItem);
        verify(todoRepository,times(1)).save(updateTodoItem);
    }

   /* @Test
    public void deleteCompletedTodoItems(){
        List<ToDoItem> toDoItems = new ArrayList<ToDoItem>();
        ToDoItem itemOne = constructTodoItem(1l,"todoItem 1",true);
        ToDoItem itemTwo= constructTodoItem(2l,"todoItem 2",true);
        toDoItems.add(itemOne);
        toDoItems.add(itemTwo);

        when(todoRepository.findByComplete(true)).thenReturn(toDoItems);
        service.deleteCompletedItems();
    }*/


    public ToDoItem constructTodoItem(Long id, String description, boolean complete){
        ToDoItem item = new ToDoItem();
        item.setId(id);
        item.setDescription(description);
        item.setComplete(complete);
        return item;
     }
}
